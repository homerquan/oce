'use strict';

angular.module('oceApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('insight', {
        url: '/insight',
        templateUrl: 'app/insight/insight.html',
        controller: 'InsightCtrl'
      });
  });