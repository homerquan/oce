'use strict';

angular.module('oceApp')
  .controller('MainCtrl', function($scope, $http, socket) {
    $scope.answers = [];
    $http.get('/api/answers').success(function(answers) {
      $scope.answers = answers;
      $scope.students = _.uniq(_.pluck(answers, 'student'));
      $scope.selectedStudent = $scope.students[0];
      $scope.exercises = _.uniq(_.pluck(answers, 'exercise'));
      $scope.selectedExercise = $scope.exercises[0];
      $scope.labels = $scope.students;
      $scope.series = ['total', 'correct', 'incorrect'];
      $scope.updateExercise = function(exercise) {
        var totalSerie = $scope.students.map(function(item) {
          var total = _.where(answers, {
            student: item,
            exercise: exercise
          }).length;
          return total
        });
        var correctSerie = $scope.students.map(function(item) {
          var correct = _.where(answers, {
            student: item,
            exercise: exercise,
            result: 1
          }).length;
          return correct
        });
        var incorrectSerie = $scope.students.map(function(item) {
          var incorrect = _.where(answers, {
            student: item,
            exercise: exercise,
            result: -1
          }).length;
          return incorrect
        });

        $scope.singleData = [totalSerie, correctSerie, incorrectSerie];
        var dsTotal=_.reduce(totalSerie, function(memo, num){ return memo + num; }, 0);
        var dsCorrect=_.reduce(correctSerie, function(memo, num){ return memo + num; }, 0);
        var dsIncorrect=_.reduce(incorrectSerie, function(memo, num){ return memo + num; }, 0);
        $scope.singleDataPie = [dsTotal*100, dsCorrect*100, dsIncorrect*100];
      };

      //show all
      var totalSerie = $scope.students.map(function(item) {
        var total = _.where(answers, {
            student: item
          }).length;
          return total
        });
        var correctSerie = $scope.students.map(function(item) {
          var correct = _.where(answers, {
            student: item,
            result: 1
          }).length;
          return correct
        });
        var incorrectSerie = $scope.students.map(function(item) {
          var incorrect = _.where(answers, {
            student: item,
            result: -1
          }).length;
          return incorrect
        });
        $scope.data = [totalSerie, correctSerie, incorrectSerie];

        //show single exercise
        $scope.updateExercise($scope.selectedExercise);
        socket.syncUpdates('answers', $scope.answers);
    });
  });