db.dropDatabase();
db.createCollection('answers');
db.answers.insert({
    "student" : "abc",
    "exercise" : "123",
    "result" : -1
});
db.answers.insert({
    "student" : "abc",
    "exercise" : "456",
    "result" : 1
});
db.answers.insert({
    "student" : "def",
    "exercise" : "123",
    "result" : 1
});