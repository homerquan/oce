'use strict';

var _ = require('lodash');
var Answer = require('../answer/answer.model');

// Get list of querys
exports.index = function(req, res) {
  if(req.query.action) {
    if (req.query.action === 'answers') {
       // TODO find by class
       Answer.find().exec(function(err, docs){
           var results=[];
           return res.status(200).json(docs);
       });
    } else {
      return handleError(res, "not support action");
    }
  } else {
    return handleError(res, "no action");
  }
 
};


function handleError(res, err) {
  return res.status(500).send(err);
}