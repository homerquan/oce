'use strict';

var mongoose = require('mongoose'),
	timestamps = require('mongoose-timestamp'),
	Schema = mongoose.Schema;

var AnswerSchema = new Schema({
	student: {
		type: String,
		index: true,
		required: true 
	},
	exercise: {
		type: String,
		index: true,
		required: true
	},
	result: {
		type:Number,
		required: true
	}
});

AnswerSchema.static('queryByExercise', function(exe, cb) {
	this.findOne({
		exercise: exe
	}).aggregate([
        {
            $group: {
                _id: '$region',  //$region is the column name in collection
                count: {$sum: 1}
            }
        }
    ], function (err, result) {
        if (err) {
            next(err);
        } else {
            res.json(result);
        }
    }).exec(cb);
});

AnswerSchema.plugin(timestamps);
module.exports = mongoose.model('Answer', AnswerSchema);